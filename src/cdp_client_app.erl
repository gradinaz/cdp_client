%%%-------------------------------------------------------------------
%% @doc  public API
%% @end
%%%-------------------------------------------------------------------

-module(cdp_client_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

%%====================================================================
%% API
%%====================================================================
%%--------------------------------------------------------------------
%% @spec start(Type, StartArgs) -> {ok, Pid} |
%%                                 {ok, Pid, State} |
%%                                 {error, Reason}
%% @doc This function is called whenever an application
%% is started using application:start/1,2, and should start the processes
%% of the application. If the application is structured according to the
%% OTP design principles as a supervision tree, this means starting the
%% top supervisor of the tree.
%% @end

-spec start(term(), term()) -> {ok, pid()} | {error, any()}.
start(_StartType, _StartArgs) ->
    cdp_client_sup:start_link().

%%--------------------------------------------------------------------
%%--------------------------------------------------------------------
%% @spec stop(State) -> void()
%% @doc This function is called whenever an application
%% has stopped. It is intended to be the opposite of Module:start/2 and
%% should do any necessary cleaning up. The return value is ignored.
%% @end
%%--------------------------------------------------------------------
-spec stop(term()) -> ok.
stop(_State) ->
    ok.
%%====================================================================
%% Internal functions
%%====================================================================
