%%%-------------------------------------------------------------------
%% @doc cdp_client top level supervisor.
%% @end
%%%-------------------------------------------------------------------

-module(cdp_client_sup).

-behaviour(supervisor).

%% API
-export([start_link/0]).

%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%====================================================================
%% API functions
%%====================================================================
-spec start_link() -> {ok, pid()} | ignore | {error, any()}.
start_link() ->
    supervisor:start_link({local, ?SERVER}, ?MODULE, []).

%%====================================================================
%% Supervisor callbacks
%%====================================================================
-spec init(any()) -> {ok, {supervisor:sup_flags(), [supervisor:child_spec()]}} | ignore.
%% Child :: {Id,StartFunc,Restart,Shutdown,Type,Modules}
init([]) ->
    Host = application:get_env(cdp_client, host, "localhost"),
    Port = application:get_env(cdp_client, port, 9222),
    Childs = [
        {cdp_client_conn_srv, {cdp_client_conn_srv, start_link, [Host, Port]}, permanent, 5000, worker, [cdp_client_conn_srv]},
        {cdp_client_storage_srv,
            {cdp_client_storage_srv, start_link, []}, permanent, 5000, worker, [cdp_client_storage_srv]}
    ],
    {ok, { {one_for_all, 5, 10}, Childs} }.
