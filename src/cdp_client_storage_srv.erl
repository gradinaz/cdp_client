%% @author Sergiy Vasylchuk

-module(cdp_client_storage_srv).

-behaviour(gen_server).

%%% API
-export([
    start_link/0,
    store_product_price/2
]).

%%% gen_server
-export([
    init/1,
    handle_call/3,
    handle_cast/2,
    handle_info/2,
    terminate/2,
    code_change/3
]).

-include_lib("kernel/include/logger.hrl").

-define(SERVER, ?MODULE).

-record(state, {
    ref :: ets:tid()
}).

%%%====================================================================================================================
%%% API
%%%====================================================================================================================
store_product_price(ProductId, Price) ->
    gen_server:call(?MODULE, {store, ProductId, Price}).
%%%====================================================================================================================
%%% gen_server
%%%====================================================================================================================
-spec start_link() -> {ok, Pid :: pid()} | ignore | {error, Reason :: term()}.
start_link() ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).


%%---------------------------------------------------------------------------------------------------------------------
-spec init(Args :: term()) -> {ok, State :: #state{}}.
init([]) ->
    Ref = ets:new(?MODULE, [set]),
    {ok, #state{ref = Ref}}.

%%---------------------------------------------------------------------------------------------------------------------
-spec handle_call(Request :: term(), From :: {pid(), Tag :: term()}, State :: #state{}) ->
    {reply, Reply :: term(), NewState :: #state{}}.
handle_call({store, ProductId, Price}, _From, #state{ref = Ref} = State) ->
    true = ets:insert(Ref, {ProductId, Price}),
    ?LOG_INFO("ProductId: ~p Price: ~p~n", [ProductId, Price]),
    {reply, ok, State}.


%%---------------------------------------------------------------------------------------------------------------------
-spec handle_cast(Request :: term(), State :: #state{}) ->
    {noreply, NewState :: #state{}}.
handle_cast(_Info, State) ->
    {noreply, State}.


%%---------------------------------------------------------------------------------------------------------------------
-spec handle_info(Info :: term(), State :: #state{}) ->
    {noreply, NewState :: #state{}}.
handle_info(_Info, State) ->
    {noreply, State}.

%%---------------------------------------------------------------------------------------------------------------------
-spec terminate(Reason :: (normal | shutdown | {shutdown, term()} | term()), State :: #state{}) -> ok.
terminate(_Reason, _State) ->
    ok.

%%---------------------------------------------------------------------------------------------------------------------
-spec code_change(OldVsn :: term() | {down, term()}, State :: #state{}, Extra :: term()) ->
    {ok, NewState :: #state{}}.
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.
%%---------------------------------------------------------------------------------------------------------------------

