%% @author Sergiy Vasylchuk

-module(cdp_client_conn_srv).

-behaviour(gen_server).

%%% API
-export([start_link/2]).

%%% gen_server
-export([
    init/1,
    handle_call/3,
    handle_cast/2,
    handle_info/2,
    terminate/2,
    code_change/3
]).

-include_lib("kernel/include/logger.hrl").

-define(SERVER, ?MODULE).
-define(NEEDED_URL, <<"https://pro.coinbase.com/trade/">>).

-record(state, {
    page_id :: binary(),
    connection :: pid(),
    timer_ref :: timer:tref() | undefined
}).

%%%====================================================================================================================
%%% API
%%%====================================================================================================================
%%%====================================================================================================================
%%% gen_server
%%%====================================================================================================================
-spec start_link(Host::string(), Port::non_neg_integer()) -> {ok, Pid :: pid()} | ignore | {error, Reason :: term()}.
start_link(Host, Port) ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, {Host, Port}, []).


%%---------------------------------------------------------------------------------------------------------------------
-spec init(Args :: term()) -> {ok, State :: #state{}}.
init({Host, Port}) ->
    try
        ByteSize = byte_size(?NEEDED_URL),
        {ok, {_, _, Res}} = httpc:request("http://" ++ Host ++ ":" ++ integer_to_list(Port) ++ "/json"),
        Params = jsx:decode(list_to_binary(Res), [return_maps]),
        case find_page_id(ByteSize, Params) of
            undefined ->
                ?LOG_ERROR("Could not find needed page id"),
                {stop, {error, undefined}};
            PageId ->
                {ok, Conn} = gun:open(Host, Port),
                _ = gun:ws_upgrade(Conn, <<"/devtools/page/", PageId/binary>>),
                {ok, #state{page_id = PageId, connection = Conn}}
        end
    catch C:E:S ->
        ?LOG_ERROR("Error while attempting to start DevTools connector: ~p~n", [{C, E}]),
        {stop, {C, E, S}}
    end.

%%---------------------------------------------------------------------------------------------------------------------
-spec handle_call(Request :: term(), From :: {pid(), Tag :: term()}, State :: #state{}) ->
    {reply, Reply :: term(), NewState :: #state{}}.
handle_call(_Request, _From, State) ->
    {reply, ok, State}.


%%---------------------------------------------------------------------------------------------------------------------
-spec handle_cast(Request :: term(), State :: #state{}) ->
    {noreply, NewState :: #state{}}.
handle_cast(_Info, State) ->
    {noreply, State}.


%%--------------------------------------------------------------------
-spec(handle_info(Info :: timeout() | term(), State :: #state{}) ->
    {noreply, NewState :: #state{}} |
    {noreply, NewState :: #state{}, timeout() | hibernate} |
    {stop, Reason :: term(), NewState :: #state{}}).
handle_info(ping_ws, #state{connection = C} = State) ->
    ok = gun:ws_send(C, ping),
    {noreply, State};
handle_info({gun_ws, C, _, {_, Content}}, #state{connection = C} = State) ->
    DecodedFrame = jsx:decode(Content, [return_maps]),
    Method = maps:get(<<"method">>, DecodedFrame, undefined),
    _ = handle_devtools(Method, DecodedFrame),
    {noreply, State};
handle_info({gun_upgrade, C, _, [<<"websocket">>], _}, #state{connection = C} = State) ->
    ok = gun:ws_send(C, {text, <<"{\"id\":1,\"method\":\"Network.enable\",\"params\":{\"maxPostDataSize\":65536}}">>}),
    ?LOG_INFO("Connected to DevTools!~n", []),
    {ok, TRef} = timer:send_interval(30000, ping_ws),
    {noreply, State#state{timer_ref = TRef}};
handle_info({gun_response, C, _, _, _, _}, #state{connection = C}) ->
    {stop, {error, ws_upgrade_failed}};
handle_info({gun_error, C, _, _}, #state{connection = C}) ->
    {stop, {error, ws_upgrade_failed}};
handle_info(_Info, State) ->
    {noreply, State}.

%%--------------------------------------------------------------------
%%--------------------------------------------------------------------
-spec(terminate(Reason :: (normal | shutdown | {shutdown, term()} | term()),
    State :: #state{}) -> term()).
terminate(_Reason, #state{timer_ref = T}) ->
    case T of
        undefined -> noop;
        T -> timer:cancel(T)
    end,
    ok.

%%---------------------------------------------------------------------------------------------------------------------
-spec code_change(OldVsn :: term() | {down, term()}, State :: #state{}, Extra :: term()) ->
    {ok, NewState :: #state{}}.
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.
%%---------------------------------------------------------------------------------------------------------------------
%%%===================================================================
%%% Internal functions
%%%===================================================================
-spec handle_devtools(binary(), map()) -> ok.
handle_devtools(<<"Network.webSocketFrameReceived">>, #{<<"params">> := #{
    <<"response">> := #{
        <<"payloadData">> := Payload
    }
}}) ->
    #{<<"type">> := Type} = CoinbaseMessage = jsx:decode(Payload, [return_maps]),
    _ = handle_coinbase(Type, CoinbaseMessage),
    ok;
handle_devtools(_, _) -> ok.

-spec handle_coinbase(binary(), map()) -> ok.
handle_coinbase(<<"ticker">>, #{<<"product_id">> := ProductId, <<"price">> := Price} = _Acc)->
    ok = cdp_client_storage_srv:store_product_price(ProductId, convert_price(Price));
handle_coinbase(_, _) -> ok.


-spec find_page_id(ByteSize::integer(), Params::[map()]) -> Page::binary() | undefined.
find_page_id(ByteSize, Params) ->
    lists:foldl(fun(#{<<"url">> := Url, <<"id">> := PageId}, undefined) ->
        case binary:part(Url, 0, ByteSize) of
            <<"https://pro.coinbase.com/trade/">> -> PageId;
            _ -> undefined
        end;
        (_, Acc) -> Acc
                end, undefined, Params).

-spec convert_price(Price::binary()) -> integer() | float().
convert_price(Price) ->
    try
        binary_to_float(Price)
    catch
        C:E:_S ->
            ?LOG_INFO("Bad format of price. Price not float: ~p~n", [{C, E}]),
            binary_to_integer(Price)
    end.