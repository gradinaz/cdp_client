# cdp_client

* Edit `env/local.config` and change `host` and `port` according to your environment
* You should start your host Chrome instance with the remote-debugging-port command line. Example: 
```
chromium --remote-debugging-port=9222 https://needed_site.com
```
* Launch Rebar shell:
```
$ rebar3 shell
```